import React from 'react';
import Toggle from './index';

describe('Toggle', () => {
  it('renders correctly', () => {
    // Add a snapshot test for rendering the component using
    // shallow rendering.
      const wrapper = shallow(<Toggle />)
      expect(wrapper).toMatchSnapshot()
  });

  it('is toggled off by default', () => {
    // Add a test that inspects the initial state and validates that
    // it is "off" by default if not specified otherwise using props.
      const wrapper = shallow(<Toggle/>)
      expect(wrapper).toHaveState('isOn', false)
  });

  it('uses the "initialState" prop', () => {
    // Add a test that validates that the initial state matches the
    // given prop.
      const component = shallow(<Toggle initialState={true}/>)
      expect(component).toHaveState('isOn', true)
  });

  it('sets the correct css class name initially', () => {
    // Add a test that checks that the component renders the 'off'
    // class when rendered initially.
      const component = shallow(<Toggle/>)
      expect(component).toHaveClassName('off')
  });

  it('sets the toggle to "on" on click', () => {
    // Add a test that simulates a click and checks if the toggle
    // switches to "on" when clicked.
      const component = shallow(<Toggle/>)
      expect(component).toHaveState('isOn', false)
      component.find('button').simulate('click')
      expect(component).toHaveState('isOn', true)
  });

  it('calls the passed click callback', () => {
    // Add a test with a "spy" (mock function) for the click callback
    // and validate that it is called with the correct argument.
      const callback = jest.fn()
      const component = shallow(<Toggle onToggle={callback}/>)
      component.find('button').simulate('click')
      expect(callback).toHaveBeenCalledWith(true)
  });
});
